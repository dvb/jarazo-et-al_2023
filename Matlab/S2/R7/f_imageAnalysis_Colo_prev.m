function [ObjectsThisOrganoid] = f_imageAnalysis_Colo_prev(Label,ch1,ch2,ch3,ch4,PreviewPath,Sample,ObjectsThisOrganoid,C19Mask,THMask,SNCA_Mask)

    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch3, 3)/2);
    % Scalebar
    imSize = [size(ch3, 1), size(ch3, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
%% Colocalization Masks
ColoC19andTH = C19Mask & THMask;
ColoC19andSNCA = SNCA_Mask & C19Mask;
ColoTHandSNCA = SNCA_Mask & THMask;
ColoTHSNCAC19 = SNCA_Mask & THMask & C19Mask;

 %% Previews
    RGB_C19_1p = cat (3,  imadjust(ch3(:,:,Midplane),[0,0.025],[0,1]), imadjust(ch2(:,:,Midplane),[0.001,0.015],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.05],[0,1]));
    RGB_C19_1p = imoverlay2(RGB_C19_1p, BarMask, [1 1 1]);
    RGB_C19_all = cat (3, imadjust(max(ch3,[],3),[0,0.025],[0,1]), imadjust(max(ch2,[],3),[0.001,0.015],[0,1]), imadjust(max(ch1,[],3),[0,0.05],[0,1]));
    RGB_C19_all = imoverlay2(RGB_C19_all, BarMask, [1 1 1]);
    RGB_THC19_1p = cat (3,  imadjust(ch3(:,:,Midplane),[0,0.03],[0,1]),imadjust(ch4(:,:,Midplane),[0.0015,0.015],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.05],[0,1]));
    RGB_THC19_1p = imoverlay2(RGB_THC19_1p, BarMask, [1 1 1]);
    RGB_THC19_all = cat (3,  imadjust(max(ch3,[],3),[0,0.03],[0,1]),imadjust(max(ch4,[],3),[0.0015,0.015],[0,1]), imadjust(max(ch1,[],3),[0,0.05],[0,1]));
    RGB_THC19_all = imoverlay2(RGB_THC19_all, BarMask, [1 1 1]);
    RGB_TH_1p = cat (3,  imadjust(ch4(:,:,Midplane),[0.001,0.015],[0,1]), imadjust(ch2(:,:,Midplane),[0.001,0.015],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.05],[0,1]));
    RGB_TH_1p = imoverlay2(RGB_TH_1p, BarMask, [1 1 1]);
    RGB_TH_all = cat (3,  imadjust(max(ch4,[],3),[0.001,0.015],[0,1]), imadjust(max(ch2,[],3),[0.001,0.015],[0,1]), imadjust(max(ch1,[],3),[0,0.05],[0,1]));
    RGB_TH_all = imoverlay2(RGB_TH_all, BarMask, [1 1 1]);
    RGB_all_1p = cat (3,  imadjust(ch4(:,:,Midplane),[0.001,0.015],[0,1]), imadjust(ch2(:,:,Midplane),[0.001,0.015],[0,1]),imadjust(ch3(:,:,Midplane),[0,0.025],[0,1]));
    RGB_all_1p = imoverlay2(RGB_all_1p, BarMask, [1 1 1]);
    RGB_all = cat (3,  imadjust(max(ch4,[],3),[0.001,0.015],[0,1]), imadjust(max(ch2,[],3),[0.001,0.015],[0,1]), imadjust(max(ch3,[],3),[0,0.025],[0,1]));
    RGB_all = imoverlay2(RGB_all, BarMask, [1 1 1]);
    %imtool(RGB_C19_1p)
    %imtool(RGB_C19_all)
    %imtool(RGB_TH_1p)
    %imtool(RGB_TH_all)
    %imtool(RGB_THC19_1p)
    %imtool(RGB_THC19_all)
    %imtool(RGB_all)
    
    imwrite(RGB_C19_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_C19_1p', '.png'])
    imwrite(RGB_C19_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_C19_all', '.png'])
    imwrite(RGB_TH_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_TH_1p', '.png'])
    imwrite(RGB_TH_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_TH_all', '.png'])
    imwrite(RGB_THC19_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_THC19_1p', '.png'])
    imwrite(RGB_THC19_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_THC19_all', '.png'])
    imwrite(RGB_all_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_all_1p', '.png'])
    imwrite(RGB_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_all', '.png'])
     %% Feature extraction
    ObjectsThisOrganoid.ColoC19andTH = sum(ColoC19andTH(:));
    ObjectsThisOrganoid.ColoC19andSNCA = sum(ColoC19andSNCA(:));
    ObjectsThisOrganoid.ColoTHandSNCA = sum(ColoTHandSNCA(:));
    ObjectsThisOrganoid.ColoTHSNCAC19 = sum(ColoTHSNCAC19(:));
    ObjectsThisOrganoid.ColoC19andTHNormSNCA = sum(ColoC19andTH(:))/ sum(SNCA_Mask(:));
    ObjectsThisOrganoid.ColoC19andSNCANormSNCA = sum(ColoC19andSNCA(:))/ sum(SNCA_Mask(:));
    ObjectsThisOrganoid.ColoC19andTHNormNuc = sum(ColoC19andTH(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoC19andSNCANormNuc = sum(ColoC19andSNCA(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoC19andSNCANormSNCA = sum(ColoC19andSNCA(:))/ sum(SNCA_Mask(:));
    ObjectsThisOrganoid.ColoC19andTHNormNuc = sum(ColoC19andTH(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoTHandSNCANormSNCA = sum(ColoTHandSNCA(:))/ sum(SNCA_Mask(:));
    ObjectsThisOrganoid.ColoTHandSNCANormNuc = sum(ColoTHandSNCA(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoTHSNCAC19NormSNCA = sum(ColoTHSNCAC19(:))/ sum(SNCA_Mask(:));
    ObjectsThisOrganoid.ColoTHSNCAC19NormNuc = sum(ColoTHSNCAC19(:))/ ObjectsThisOrganoid.NucMaskAlive;
    %% Colocalization Previews
    if sum(ColoC19andTH(:))> 0
      RGB_ColoC19andTHper = imoverlay(RGB_THC19_all, bwperim(max(ColoC19andTH,[],3)), [1 1 1]); 
      RGB_ColoC19andTHarea = imoverlay(RGB_THC19_all, max(ColoC19andTH,[],3), [1 1 1]); 
      filename_RGB_ColoC19andTHper = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19THAllper', '.png'];
      filename_RGB_ColoC19andTHar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19THAllar', '.png'];
      imwrite(RGB_ColoC19andTHper,filename_RGB_ColoC19andTHper);
      imwrite(RGB_ColoC19andTHarea,filename_RGB_ColoC19andTHar);
    else
    end
    if sum(ColoC19andSNCA(:))> 0
      RGB_ColoC19andSNCAper = imoverlay(RGB_C19_all, bwperim(max(ColoC19andSNCA,[],3)), [1 1 1]); 
      RGB_ColoC19andSNCAarea = imoverlay(RGB_C19_all, max(ColoC19andSNCA,[],3), [1 1 1]); 
      filename_RGB_ColoC19andSNCAper = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19SNCAAllper', '.png'];
      filename_RGB_ColoC19andSNCAar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19SNCAAllar', '.png'];
      imwrite(RGB_ColoC19andSNCAper,filename_RGB_ColoC19andSNCAper);
      imwrite(RGB_ColoC19andSNCAarea,filename_RGB_ColoC19andSNCAar);
    else
    end
    if sum(ColoTHandSNCA(:))> 0
      RGB_ColoTHandSNCAper = imoverlay(RGB_TH_all, bwperim(max(ColoTHandSNCA,[],3)), [1 1 1]); 
      RGB_ColoTHandSNCAarea = imoverlay(RGB_TH_all, max(ColoTHandSNCA,[],3), [1 1 1]); 
      filename_RGB_ColoTHandSNCAper = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoTHSNCAAllper', '.png'];
      filename_RGB_ColoTHandSNCAar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoTHSNCAAllar', '.png'];
      imwrite(RGB_ColoTHandSNCAper,filename_RGB_ColoTHandSNCAper);
      imwrite(RGB_ColoTHandSNCAarea,filename_RGB_ColoTHandSNCAar);
    else
    end
    if sum(ColoTHSNCAC19(:))> 0
      RGB_ColoTHSNCAC19per = imoverlay(RGB_all, bwperim(max(ColoTHSNCAC19,[],3)), [1 1 1]); 
      RGB_ColoTHSNCAC19area = imoverlay(RGB_all, max(ColoTHSNCAC19,[],3), [1 1 1]); 
      filename_RGB_ColoTHSNCAC19per = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoAllper', '.png'];
      filename_RGB_ColoTHSNCAC19ar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoAllar', '.png'];
      imwrite(RGB_ColoTHSNCAC19per,filename_RGB_ColoTHSNCAC19per);
      imwrite(RGB_ColoTHSNCAC19area,filename_RGB_ColoTHSNCAC19ar);
    else
    end
end

