function [ObjectsThisOrganoid,THMask] = f_imageAnalysis_ch4_c(Label,ch4,PreviewPath,Sample,ObjectsThisOrganoid,OrganoidMaskArea)

    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch4, 3)/2);
    % Scalebar
    imSize = [size(ch4, 1), size(ch4, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    
    %% TH (ch4)
    TH_FT = zeros(size(ch4), 'double');

    parfor p=1:size(ch4, 3)
        %TH_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [7,1], 0);
        TH_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
    end
    %vol(TH_FT*100,0,1,'hot')
    THMask = TH_FT > 0.0025;% vol(THMask)
    clear 'TH_FT'
    
    THMask = bwareaopen(THMask, 50);
    THMask = THMask & OrganoidMaskArea;
    THMask = medfilt3(THMask);
    %vol(THMask)   
 %%TH Fragmentation
    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceTH = THMask & ~(imerode(THMask, Conn6));
    %vol(SurfaceTH)
%%Collect and make previews of TH
    ObjectsThisOrganoid.THMaskSum = sum(THMask(:));
    ObjectsThisOrganoid.THByNuc = sum(THMask(:)) /  ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.THByNucAlive = sum(THMask(:)) /  ObjectsThisOrganoid.NucMaskSum;
	ObjectsThisOrganoid.THFragmentation = sum(SurfaceTH(:)) /  ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.THBySNCA = ObjectsThisOrganoid.THMaskSum / ObjectsThisOrganoid.SNCAMaskSum;
 
%Make previews TH    
    PreviewTH = imadjust(ch4(:,:,Midplane),[0.001,0.015],[0,1]);%imtool(PreviewTH)
    PreviewTHallp = imadjust(max(ch4,[],3),[0.001,0.015],[0,1]);%imtool(PreviewTHallp)
    
    PreviewTHArea1p = imoverlay2(PreviewTH,THMask(:,:,Midplane), [1 0 0]);
    PreviewTHArea1p = imoverlay2(PreviewTHArea1p, BarMask, [1 1 1]);
	PreviewTHPerim1p = imoverlay2(PreviewTH, bwperim(THMask(:,:,Midplane)), [1 0 0]);
    PreviewTHPerim1p = imoverlay2(PreviewTHPerim1p, BarMask, [1 1 1]);
   
    PreviewTHAreaMIP = imoverlay2(PreviewTHallp,max(THMask,[],3), [1 0 0]);
    PreviewTHAreaMIP = imoverlay2(PreviewTHAreaMIP, BarMask, [1 1 1]);
	PreviewTHPerimMIP = imoverlay2(PreviewTHallp, bwperim(max(THMask,[],3)), [1 0 0]);
    PreviewTHPerimMIP = imoverlay2(PreviewTHPerimMIP, BarMask, [1 1 1]);
    
   
    imwrite(PreviewTH, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'TH_raw_MIP', '.png'])
    imwrite(PreviewTHallp, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'TH_raw_1p', '.png'])
    imwrite(PreviewTHArea1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'TH_Area_1p', '.png'])
    imwrite(PreviewTHPerim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'TH_Perim_1p', '.png'])
    imwrite(PreviewTHAreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'TH_Area_MIP', '.png'])
    imwrite(PreviewTHPerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'TH_Perim_MIP', '.png'])
   
end

