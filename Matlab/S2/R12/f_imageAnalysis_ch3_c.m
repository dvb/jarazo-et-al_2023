function [ObjectsThisOrganoid,C19Mask] = f_imageAnalysis_ch3_c(Label,ch3,PreviewPath,Sample,ObjectsThisOrganoid,OrganoidMaskArea)

    IdentityString = num2str(Label);
     %% Previews 
    % Middle plane
    Midplane = round(size(ch3, 3)/2);
    % Scalebar
    imSize = [size(ch3, 1), size(ch3, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    
    %% C19 (ch3)
    C19_FT = zeros(size(ch3), 'double');

    parfor p=1:size(ch3, 3)
        %C19_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,3], 0);
        C19_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
    end
    %vol(C19_FT*100,0,1,'hot')
    C19Mask = C19_FT > 0.008;% vol(C19Mask)
    clear 'C19_FT'

    C19Mask = bwareaopen(C19Mask, 250);
    C19Mask = C19Mask & OrganoidMaskArea;
    C19Mask =medfilt3(C19Mask);
    %vol(C19Mask)   
    %vol(C19_FT*100,0,1,'hot')    
    
%%Collect and make previews of C19
%Collect info C19
    ObjectsThisOrganoid.C19MaskSum = sum(C19Mask(:)); 
    ObjectsThisOrganoid.C19ByNuc = sum(C19Mask(:))/ ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.C19ByNucAlive = sum(C19Mask(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.C19ByTH = sum(C19Mask(:))/ ObjectsThisOrganoid.THMaskSum;
    ObjectsThisOrganoid.C19BySNCA = sum(C19Mask(:))/ ObjectsThisOrganoid.SNCAMaskSum;

%Make Previews mono    
    PreviewC19 = imadjust(ch3(:,:,Midplane),[0,0.1],[0,1]);%imtool(PreviewC19)
    PreviewC19allp = imadjust(max(ch3,[],3),[0,0.1],[0,1]);%imtool(PreviewC19allp)
    
    PreviewC19Area1p = imoverlay2(PreviewC19,C19Mask(:,:,Midplane), [1 0 0]);
    PreviewC19Area1p = imoverlay2(PreviewC19Area1p, BarMask, [1 1 1]);
	PreviewC19Perim1p = imoverlay2(PreviewC19, bwperim(C19Mask(:,:,Midplane)), [1 0 0]);
    PreviewC19Perim1p = imoverlay2(PreviewC19Perim1p, BarMask, [1 1 1]);
    
    PreviewC19AreaMIP = imoverlay2(PreviewC19allp,max(C19Mask,[],3), [1 0 0]);
    PreviewC19AreaMIP = imoverlay2(PreviewC19AreaMIP, BarMask, [1 1 1]);
	PreviewC19PerimMIP = imoverlay2(PreviewC19allp, bwperim(max(C19Mask,[],3)), [1 0 0]);
    PreviewC19PerimMIP = imoverlay2(PreviewC19PerimMIP, BarMask, [1 1 1]);
        
    imwrite(PreviewC19, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'C19_raw_1p', '.png'])
    imwrite(PreviewC19allp, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'C19_raw_MIP', '.png'])
    imwrite(PreviewC19Area1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'C19_Area_1p', '.png'])
    imwrite(PreviewC19Perim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'C19_Perim_1p', '.png'])
    imwrite(PreviewC19AreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'C19_Area_MIP', '.png'])
    imwrite(PreviewC19PerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'C19_Perim_MIP', '.png'])
end

