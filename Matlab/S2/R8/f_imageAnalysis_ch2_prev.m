function [ObjectsThisOrganoid,SNCA_Mask] = f_imageAnalysis_ch2_prev(Label,ch2,PreviewPath,Sample, ObjectsThisOrganoid,OrganoidMaskArea)
    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch2, 3)/2);
    % Scalebar
    imSize = [size(ch2, 1), size(ch2, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    %% SNCA (ch2)
    SNCA_FT = zeros(size(ch2), 'double');

    parfor p=1:size(ch2, 3)
        SNCA_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %SNCAconfo_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
    end
    %vol(SNCA_FT*10, 0, 1,'hot') % imtool(SNCA_FT(:,:,5))
    %vol(SNCA_FT*1000, 0, 1)
    %SNCAMask = SNCA_FT > 0.0025; %vol(SNCAMask, 0, 1)
    SNCAMask = SNCA_FT > 0.005;
    clear 'SNCA_FT'
    SNCAMask = bwareaopen(SNCAMask, 50);
    SNCAMask = SNCAMask & OrganoidMaskArea;
    SNCAMask =medfilt3(SNCAMask);
    SNCA_Mask = SNCAMask;
        
    %%SNCA Fragmentation
    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceSNCA = SNCA_Mask & ~(imerode(SNCA_Mask, Conn6));
    %vol(SurfaceSNCA)
%%Collect and make previews of SNCA  
%Collect info SNCA   
    ObjectsThisOrganoid.SNCAMaskSum = sum(SNCA_Mask(:));
    ObjectsThisOrganoid.SNCAByNuc = sum(SNCA_Mask(:)) /  ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.SNCAByNucAlive = sum(SNCA_Mask(:)) / ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.SNCAFragmentation = sum(SurfaceSNCA(:)) /  ObjectsThisOrganoid.NucMaskSum;

%Make previews SNCA
    PreviewSNCA = imadjust(ch2(:,:,Midplane),[0.001,0.01],[0,1]);%imtool(PreviewSNCA)
    PreviewSNCAallp = imadjust(max(ch2,[],3),[0.001,0.01],[0,1]);%imtool(PreviewSNCAallp)
    
    PreviewSNCAArea1p = imoverlay2(PreviewSNCA,SNCA_Mask(:,:,Midplane), [1 0 0]);
    PreviewSNCAArea1p = imoverlay2(PreviewSNCAArea1p, BarMask, [1 1 1]);
    PreviewSNCAPerim1p = imoverlay2(PreviewSNCA, bwperim(SNCA_Mask(:,:,Midplane)), [1 0 0]);
    PreviewSNCAPerim1p = imoverlay2(PreviewSNCAPerim1p, BarMask, [1 1 1]);
    
    PreviewSNCAAreaMIP = imoverlay2(PreviewSNCAallp,max(SNCA_Mask,[],3), [1 0 0]);
    PreviewSNCAAreaMIP = imoverlay2(PreviewSNCAAreaMIP, BarMask, [1 1 1]);
    PreviewSNCAMIPerimMIP = imoverlay2(PreviewSNCAallp, bwperim(max(SNCA_Mask,[],3)), [1 0 0]);
    PreviewSNCAMIPerimMIP = imoverlay2(PreviewSNCAMIPerimMIP, BarMask, [1 1 1]);
       
    imwrite(PreviewSNCA, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'SNCA_raw_1p', '.png'])
    imwrite(PreviewSNCAallp, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'SNCA_raw_MIP', '.png'])
    imwrite(PreviewSNCAArea1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'SNCA_Area_1p', '.png'])
    imwrite(PreviewSNCAPerim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'SNCA_Perim_1p', '.png'])
    imwrite(PreviewSNCAAreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'SNCA_Area_MIP', '.png'])
    imwrite(PreviewSNCAMIPerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'SNCA_Perim_MIP', '.png'])
end

