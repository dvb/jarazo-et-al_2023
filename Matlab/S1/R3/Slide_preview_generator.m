%% User inputs
delete(gcp('nocreate'))
%addpath(genpath('/mnt/lscratch/users/pantony/GitLibraries'))
addpath(genpath('/work/projects/lcsb_hcs/Library/hcsforge'))
InPath = '/scratch/users/jjarazo/HCSdata/C9/FR/MBO/R3';
DataPath = InPath;
OutPath = '/scratch/users/jjarazo/Results/C9/FR/MBO/R3';

%% Prepare folders
mkdir(OutPath)
ThumbnailPath = [OutPath, filesep, 'Thumbnails'];
mkdir(ThumbnailPath)
PreviewPath = [OutPath, filesep, 'Previews'];
mkdir(PreviewPath)

%% Load Metadata
mesFile = [InPath, filesep, 'JJ_20220131_Rescan_S1_A4_0.mes'];
%mesFile = mesFile.name;
MetaData = f_CV8000_getChannelInfo(DataPath, mesFile);
PlaneCount = str2double(MetaData.ZCount);

SlideViewPlane = round(PlaneCount/2); 
f_CV8000_CreateSlideViewFiles(MetaData, SlideViewPlane, ThumbnailPath)

    %% Reconstruct slide channel 1
    GrayRangeInput = [0 0.01]; % See imadjust range [0 1]
    SlidePreviewIms = f_CV8000_SlideViewOrganoids(ThumbnailPath, unique(MetaData.InfoTable{:}.Well), 1, GrayRangeInput);
    for s = 1:size(SlidePreviewIms, 1)
        %imtool(SlidePreviewIms{s})% Index corresponds to slide
        imwrite(SlidePreviewIms{s}, [ThumbnailPath,filesep,'SlidePreview_',num2str(s),'.png'])
    end
